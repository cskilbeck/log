﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Debugger;
using Microsoft.VisualStudio.Debugger.ComponentInterfaces;
using Microsoft.VisualStudio.Debugger.DefaultPort;
using Microsoft.VisualStudio.Debugger.Exceptions;
using Microsoft.VisualStudio.Debugger.Interop;

namespace LoggingWindow
{
    public class ExceptionTrigger : IDebugEventCallback2, IDkmExceptionTriggerHitNotification
    {
        const int MAJOR_VERSION = 1;
        const int MINOR_VERSION = 1;
        const byte version = (MAJOR_VERSION << 4) | MINOR_VERSION;

        private static readonly Lazy<ExceptionTrigger> instance = new Lazy<ExceptionTrigger>();

        public delegate void DebuggerStartedDelegate(uint process_id);
        public event DebuggerStartedDelegate DebuggerStarted;

        public bool notified = false;

        private static Guid Exception_sourceId = new Guid("fd41eacb-8199-445f-9179-a82b46e46f77");

        // TODO (chs): prefer either IPV6 or IPV4 somehow? Prefers IPV4 for now.
        private static IPAddress ipAddress;
        private static TcpListener listener = null;
        private static Thread listener_thread = null;
        private static object listener_creation_lock = new object();
        private static ServerAddress server_address;

        public static ExceptionTrigger Instance
        {
            get
            {
                return instance.Value;
            }
        }

        public static void Log(string s, params object[] args)
        {
            FileLog.Write(s, args);
        }

        static IPAddress GetLocalIPAddress(AddressFamily family)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == family)
                {
                    return ip;
                }
            }
            return null;
        }

        // ONLY InterNetwork or InterNetworkV6
        static AddressFamily default_network_family = AddressFamily.InterNetworkV6;

        public int Event(IDebugEngine2 pEngine, IDebugProcess2 pProcess, IDebugProgram2 pProgram, IDebugThread2 pThread, IDebugEvent2 pEvent, ref Guid riidEvent, uint dwAttrib)
        {
            try
            {
                Log("Debugger event " + riidEvent.ToString());

                // or... sod it, let's just kick things off when we get any kind of event

                // debugger started event
                //if ((riidEvent == typeof(IDebugProgramCreateEvent2).GUID || riidEvent == typeof(IDebugModuleLoadEvent2).GUID))

                {
                    // get the procid
                    AD_PROCESS_ID[] process_id = new AD_PROCESS_ID[1];
                    if (!notified && pProcess != null && pProcess.GetPhysicalProcessId(process_id) == VSConstants.S_OK)
                    {
                        Log("process_id = {0}", process_id[0].dwProcessId.ToString("x8"));
                        // get the procid as a guid
                        Guid processId;
                        if (pProcess.GetProcessId(out processId) == VSConstants.S_OK)
                        {
                            Log("Guid process id is {0}", processId.ToString());
                            // get the process object
                            DkmProcess dkmProcess = DkmProcess.FindProcess(processId);
                            if (dkmProcess != null)
                            {
                                var sys_info = dkmProcess.SystemInformation;

                                // use proper ipAddress if it's remote (otherwise use the loopback)
                                if ((dkmProcess.Connection.Flags & DkmTransportConnectionFlags.LocalComputer) == 0)
                                {
                                    ipAddress = GetLocalIPAddress(default_network_family);
                                    Log("It's remote, using IP address {0}", ipAddress.ToString());
                                }
                                else
                                {
                                    ipAddress = default_network_family == AddressFamily.InterNetwork ? IPAddress.Loopback : IPAddress.IPv6Loopback;
                                    Log("It's local, using IP address {0}", ipAddress.ToString());
                                }

                                // start listening on the port
                                StartListening(ipAddress);

                                // prepare the info for the client which will be sent later when they raise the exception
                                byte family = (byte)ipAddress.AddressFamily;
                                int port = ((IPEndPoint)listener.LocalEndpoint).Port;   // RACE?
                                server_address = new ServerAddress(family, version, (ushort)port, ipAddress);

                                dkmProcess.AddExceptionTrigger(Exception_sourceId,
                                    DkmExceptionCodeTrigger.Create(
                                        DkmExceptionProcessingStage.Thrown, null, DkmExceptionCategory.Win32, 0x00123456));

                                dkmProcess.AddExceptionTrigger(Exception_sourceId,
                                    DkmExceptionNameTrigger.Create(
                                        DkmExceptionProcessingStage.Thrown, null, DkmExceptionCategory.Clr, "Logging.Log.GetIPException"));

                                // setup the exception code trigger
                                DebuggerStarted?.Invoke(process_id[0].dwProcessId);
                                notified = true;
                            }
                        }
                    }
                }
            }
            finally
            {
                if (pEngine != null && Marshal.IsComObject(pEngine)) Marshal.ReleaseComObject(pEngine);
                if (pProcess != null && Marshal.IsComObject(pProcess)) Marshal.ReleaseComObject(pProcess);
                if (pProgram != null && Marshal.IsComObject(pProgram)) Marshal.ReleaseComObject(pProgram);
                if (pThread != null && Marshal.IsComObject(pThread)) Marshal.ReleaseComObject(pThread);
                if (pEvent != null && Marshal.IsComObject(pEvent)) Marshal.ReleaseComObject(pEvent);
            }
            return 0;
        }

        void IDkmExceptionTriggerHitNotification.OnExceptionTriggerHit(DkmExceptionTriggerHit hit, DkmEventDescriptorS eventDescriptor)
        {
            if(hit.Exception is Microsoft.VisualStudio.Debugger.Native.DkmWin32ExceptionInformation)
            {
                Log("Got Win32 exception");
                var exceptionInfo = hit.Exception as Microsoft.VisualStudio.Debugger.Native.DkmWin32ExceptionInformation;
                if (exceptionInfo.Code == 0x00123456)
                {
                    const int exceptionParameterCount = 2;  // enough 2 IntPtrs, 1st is len of buffer, 2nd is address of buffer
                    if (exceptionInfo.ExceptionParameters.Count >= exceptionParameterCount)
                    {
                        ulong len = exceptionInfo.ExceptionParameters[0];   // size in bytes of buffer we can write to
                        ulong addr = exceptionInfo.ExceptionParameters[1];  // target memory address of that buffer
                        hit.Process.WriteMemory(addr, MarshalAsBytes(server_address));
                    }
                    eventDescriptor.Suppress();
                }
            }
            else if (hit.Exception is Microsoft.VisualStudio.Debugger.Clr.DkmClrExceptionInformation)
            {
                Log("Got CLR exception");
                ulong addr;
                if (UInt64.TryParse(hit.AdditionalInformation, out addr))
                {
                    Log("Writing to process memory");
                    hit.Process.WriteMemory(addr, MarshalAsBytes(server_address));
                }
                else
                {
                    Log("Huh? Param was {0}", hit.AdditionalInformation);
                }
                eventDescriptor.Suppress();
            }
            else
            {
                Log("Got exception: {0}", hit.ToString());
            }
        }

        static bool ReadBytes(NetworkStream s, byte[] bytes, int size)
        {
            int got = 0;
            if(size > bytes.Length)
            {
                return false;
            }
            while (got < size)
            {
                int r = s.Read(bytes, got, size - got);
                if (r == 0)
                {
                    return false;
                }
                got += r;
            }
            return true;
        }

        static void StartListening(IPAddress address)
        {
            lock (listener_creation_lock)
            {
                StopListening();

                if (listener == null)
                {
                    listener = new TcpListener(address, 0);
                    listener.Start();// this is where it allocates the port

                    Log("Listener started, family is {0}, port is {1}", ipAddress.AddressFamily, ((IPEndPoint)listener.LocalEndpoint).Port);

                    listener_thread = new Thread(() => {

                        int log_entry_sender_size = Marshal.SizeOf<LogEntrySender>();
                        byte[] log_entry_sender_buffer = new byte[log_entry_sender_size];
                        byte[] text_buffer = new byte[4096];

                        Log("Waiting for a connection");

                        int message_count = 0;

                        try
                        {
                            var client = listener.AcceptTcpClient();

                            Log("Client connected !");

                            NetworkStream stream = client.GetStream();

                            while (true)
                            {
                                if (!ReadBytes(stream, log_entry_sender_buffer, log_entry_sender_size))
                                {
                                    break;
                                }
                                LogEntrySender log_entry_sender = UnMarshalFromBytes<LogEntrySender>(log_entry_sender_buffer);

                                if (!ReadBytes(stream, text_buffer, log_entry_sender.strings_length))
                                {
                                    break;
                                }
                                LogEntry m = LogEntryFromLogEntrySender(log_entry_sender, text_buffer);
                                LogEntry.LogWindowControl.AddLogEntry(m);
                                ++message_count;
                            }
                        }
                        catch (System.IO.IOException ioe)
                        {
                            Log("IOException: {0}", ioe.Message);
                        }
                        catch (SocketException se)
                        {
                            Log("Socket exception: {0}", se.Message);
                        }
                        catch (ThreadAbortException)
                        {
                            Log("Thread aborted!");
                        }
                        Log("Thread ended, received {0} messages", message_count);
                    });
                    listener_thread.Start();
                }
            }
        }

        static void StopListening()
        {
            Log("stop listening");
            lock (listener_creation_lock)
            {
                if (listener != null)
                {
                    Log("stop listener");
                    listener.Stop();
                    listener = null;
                }
                if (listener_thread != null)
                {
                    try
                    {
                        Log("join");
                        listener_thread.Join(100);
                    }
                    catch (TimeoutException)
                    {
                        Log("abort");
                        listener_thread.Abort();
                    }
                    listener_thread = null;
                }
            }
        }

        static byte[] MarshalAsBytes<T>(T obj)
        {
            int size = Marshal.SizeOf(obj);
            IntPtr raw = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(obj, raw, true);
            byte[] bytes = new byte[size];
            Marshal.Copy(raw, bytes, 0, size);
            Marshal.FreeHGlobal(raw);
            return bytes;
        }

        static T UnMarshalFromBytes<T>(byte[] bytes)
        {
            GCHandle gcHandle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T obj = (T)Marshal.PtrToStructure(gcHandle.AddrOfPinnedObject(), typeof(T));
            gcHandle.Free();
            return obj;
        }

        static LogEntry LogEntryFromLogEntrySender(LogEntrySender les, byte[] bytes)
        {
            int o = 0;

            string chan = new string(Encoding.UTF8.GetChars(bytes, o, les.channel_length));
            o += les.channel_length;

            string text = new string(Encoding.UTF8.GetChars(bytes, o, les.text_length));
            o += les.text_length;

            string file = new string(Encoding.UTF8.GetChars(bytes, o, les.filename_length));

            return new LogEntry()
            {
                TimeStamp = DateTime.FromFileTime(les.timestamp),
                Severity = (Severity)les.severity,
                LineNumber = les.line_number,
                Channel = chan,
                Text = text,
                Filename = file
            };
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 20)]
    public struct ServerAddress
    {
        public ServerAddress(byte family, byte version, ushort port, IPAddress address)
        {
            this.family = family;
            this.version = version;
            this.port = port;
            this.address = new byte[16];
            byte[] addr = address.GetAddressBytes();
            for(int i=0; i<addr.Length; ++i)
            {
                this.address[i] = addr[i];
            }
        }

        //PORT: 2
        [MarshalAs(UnmanagedType.U2)]
        public ushort port;

        //FAMILY: 1
        [MarshalAs(UnmanagedType.U1)]
        public byte family;                 // 0: IPV4, 1: IPV6

        //VERSION: 1
        [MarshalAs(UnmanagedType.U1)]       // Major:4 Minor:4
        public byte version;

        //IPADDRESS: 16(if it's IPV4, only the 1st 4 bytes are used)
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 16)]
        public byte[] address;
    }
}
