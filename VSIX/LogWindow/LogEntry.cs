﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace LoggingWindow
{
    public enum Severity : uint
    {
        Verbose = 0,
        Debug = 1,
        Info = 2,
        Warning = 3,
        Error = 4
    }

    public class LogEntry
    {
        public DateTime TimeStamp { get; set; }
        public Severity Severity { get; set; }
        public string Channel { get; set; }
        public string Filename { get; set; }
        public int LineNumber { get; set; }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        private string text;

        public static LogWindowControl LogWindowControl;

        private static string timestamp_format_string = "g";
        private static string timestamp_export_format_string = "yyyy-MM-dd,hh:mm:ss.fff";

        public static string[] severity_names = new string[]
        {
            "V", //Verbose = 1,
            "D", //Debug = 2,
            "I", //Info = 3,
            "W", //Warning = 4,
            "E", //Error = 5
        };

        public static string[] severity_long_names = new string[]
        {
            "VERBOSE ", //Verbose = 1,
            "DEBUG   ", //Debug = 2,
            "INFO    ", //Info = 3,
            "WARNING ", //Warning = 4,
            "ERROR   ", //Error = 5
        };

        private static Color[] severity_colors_light = new Color[]
        {
            Color.FromRgb(79, 0, 39),
            Color.FromRgb(9, 128, 0),
            Color.FromRgb(109, 86, 4),
            Color.FromRgb(0, 78, 128),
            Color.FromRgb(192,0,0)
        };

        private static Color[] severity_colors_dark = new Color[]
        {
           Color.FromRgb(220,90,220),
           Color.FromRgb(0,176,80),
           Color.FromRgb(224,160,24),
           Color.FromRgb(135,164,217),
           Color.FromRgb(255,63,63)
        };

        private static Brush[] severity_brushes = new Brush[] {
            new SolidColorBrush(severity_colors_dark[0]),
            new SolidColorBrush(severity_colors_dark[1]),
            new SolidColorBrush(severity_colors_dark[2]),
            new SolidColorBrush(severity_colors_dark[3]),
            new SolidColorBrush(severity_colors_dark[4])
        };

        private static Color[] severity_colors = severity_colors_dark;

        public static void SetThemeIsDark(bool is_dark)
        {
            severity_colors = is_dark ? severity_colors_dark : severity_colors_light;
            for (int i=0; i<(int)Severity.Error; ++i)
            {
                severity_brushes[i] = new SolidColorBrush(severity_colors[i]);
            }
            Theme.DoThemeChanged();
        }

        public static Brush GetSeverityBrush(Severity s)
        {
            return severity_brushes[Math.Min((uint)s, (uint)Severity.Error)];
        }

        public static Brush GetSeverityColor(Severity s)
        {
            return severity_brushes[Math.Min((uint)s, (uint)Severity.Error)];
        }

        public string SeverityLongName
        {
            get
            {
                return severity_long_names[Math.Min((uint)Severity, (uint)Severity.Error)];
            }
        }

        public Brush DefaultBrush
        {
            get
            {
                return LogWindowPackage.DefaultBrush;
            }
        }

        public static string TimestampFormatString
        {
            set
            {
                timestamp_format_string = value;
            }
        }

        public Brush Brush
        {
            get
            {
                return LogWindowControl.GetBrush(Channel);
            }
        }

        public Brush SeverityColorBrush
        {
            get
            {
                return GetSeverityBrush(Severity);
            }
        }

        public string SeverityName
        {
            get
            {
                return severity_names[(int)Severity];
            }
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", TimeStamp.ToString(timestamp_export_format_string), SeverityName, Escape(Channel), Escape(Text));
        }

        public string FormattedTimestamp
        {
            get
            {
                return TimeStamp.ToString(timestamp_format_string);
            }
        }

        static string Escape(string s)
        {
            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

    }

    [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 28)]
    public class LogEntrySender
    {
        [MarshalAs(UnmanagedType.U8)]
        public long timestamp;
        [MarshalAs(UnmanagedType.U4)]
        public int severity;
        [MarshalAs(UnmanagedType.U4)]
        public int line_number;
        [MarshalAs(UnmanagedType.U4)]
        public int channel_length;
        [MarshalAs(UnmanagedType.U4)]
        public int text_length;
        [MarshalAs(UnmanagedType.U4)]
        public int filename_length;

        public int strings_length
        {
            get
            {
                return channel_length + text_length + filename_length;
            }
        }

    }
}
