﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Microsoft.Internal.VisualStudio.Shell;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;

// TODO ? put the severity checkboxes in the log window somehow (or in a dropdown in the column header)

// DONE colors for VDIWE severity levels [binding done, but need to make the colors right]
// FIXED auto-scroll broken
// DONE make it work on xbox
// NOPE ?? option to output to file when not being debugged? [this goes in the client code]
// NOPE varargs in C# (callerlinenumber/callerfile getting in the way) [NOPE because string interpolation is awesome]
// DONE Help for using it [pops a webpage, need to make it better]
// DONE better way of sending log events than a pipe? [sockets]
// DONE delete button should be a little 'x'
// DONE hide delete button except when hovering
// DONE fix mouse hover formatting in channels window
// DONE auto-size last column of listview
// DONE detect solution with no channel definitions in it and create 4 defaults
// DONE settings in the solution?
// DONE hover mouse selected listviewitem background color thing (kinda, no hover now)
// DONE C++ log writer - can't get exe name (debugger events!)
// DONE Copy to csv - escape newlines
// DONE Default colors for default filters
// DONE Timestamp format option
// DONE (maybe) send less data over the pipe
// DONE WPF formatting (done for now)
// FIXED scrollbar doesn't update position dynamically
// DONE never delete the pipe! (kinda)
// DONE stick scroll to the botton when user scrolls to the botton
// DONE make it optional to clear the log when debug starts
// DONE nice logging helper class/functions
// DONE double click to line/file (almost, how to activate the editor)
// DONE save config in Project and User files
// DONE settings page in the proper place

namespace LoggingWindow
{
    [Guid(PackageGuidString)]
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [ProvideToolWindow(typeof(LogWindow), Style = VsDockStyle.Tabbed, Orientation = ToolWindowOrientation.Left, Window = EnvDTE.Constants.vsWindowKindOutput)]
    [ProvideToolWindow(typeof(LogChannels), Style = VsDockStyle.Tabbed, Orientation = ToolWindowOrientation.Left, Window = EnvDTE.Constants.vsWindowKindOutput)]
    [ProvideOptionPage(typeof(Options), "Log", "General", 0, 0, true)]
    [ProvideProfile(typeof(Options), "Log", "Settings", 106, 107, isToolsOptionPage:true, DescriptionResourceID = 108)]
    [ProvideToolWindowVisibility(typeof(LogWindow), UIContextGuids.SolutionExists)]
    [ProvideAutoLoad(UIContextGuids.SolutionExists)]
    [ProvideAutoLoad(UIContextGuids.NoSolution)]
    [ProvideAutoLoad(UIContextGuids.EmptySolution)]
    [ProvideAutoLoad(UIContextGuids.CodeWindow)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class LogWindowPackage : Package, IVsPersistSolutionProps, IVsSolutionEvents
    {
        private const string PackageGuidString = "33b6daa9-a7e5-48a8-85e0-c2382eba2095";
        private const string log_config_name = "LogWindowConfig2";
        private const string _strSolutionPersistanceKey = "LogWindowSolutionProperties_v2";
        private const string pipe_guid = "8834E49A-9D77-4166-AD66-EF29CBDA2B13";

        private static Options options;
        private static LogWindowPackage instance;

        private DebuggerEvents mySink;
        private MemoryStream stream = new MemoryStream();
        private byte[] buffer = new byte[2048];
        private LogWindow log_window;
        private LogChannels log_settings;
        private bool channels_loaded = false;
        private uint solution_events_cookie;

        public LogWindowPackage()
        {
            instance = this;
            VSColorTheme_ThemeChanged(null);
            VSColorTheme.ThemeChanged += VSColorTheme_ThemeChanged;
            DumpThemeColors();
        }

        protected override void Initialize()
        {
            FileLog.Write("Package Initialize");
            LogWindowCommand.Initialize(this);
            base.Initialize();
            LogChannelsCommand.Initialize(this);

            options = (Options)GetDialogPage(typeof(Options));
            options.PropertyChanged += Options_PropertyChanged;

            IVsSolution solution = GetService(typeof(SVsSolution)) as IVsSolution;
            if (solution != null && solution.AdviseSolutionEvents(this, out solution_events_cookie) != VSConstants.S_OK)
            {
                FileLog.Write("ERROR! Can't advise solution events");
            }

            mySink = new DebuggerEvents((IVsDebugger)GetService(typeof(IVsDebugger)));
            mySink.DebuggerStarted += DebuggerStarted;
            mySink.DebuggerEnded += DebuggerEnded;
        }

        static System.Windows.Media.Color default_channel_color = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
        static System.Windows.Media.Brush default_brush = new System.Windows.Media.SolidColorBrush(default_channel_color);

        public static System.Windows.Media.Color DefaultChannelColor
        {
            get
            {
                return default_channel_color;
            }
            set
            {
                default_channel_color = value;
                default_brush = new System.Windows.Media.SolidColorBrush(default_channel_color);
            }
        }

        public static System.Windows.Media.Brush DefaultBrush
        {
            get
            {
                return default_brush;
            }
        }

        private void VSColorTheme_ThemeChanged(ThemeChangedEventArgs e)
        {
            FileLog.Write("Theme changed!");
            DefaultChannelColor = VSColorTheme.GetThemedColor(EnvironmentColors.ToolWindowTextColorKey).ToMediaColor();
            LogEntry.LogWindowControl?.RefreshLog();
            RefreshTheme();
        }

        private void RefreshTheme()
        {
            var background = VSColorTheme.GetThemedColor(EnvironmentColors.SystemWindowColorKey);
            double lumin = background.R * 0.3 + background.G * 0.5 + background.B * 0.1;
            LogEntry.SetThemeIsDark(lumin < 128);
        }

        void CreateToolWindow<T>(ref T already) where T: class
        {
            if (already == null)
            {
                FileLog.Write("CreateToolWindow {0}", typeof(T).ToString());
                ToolWindowPane window = FindToolWindow(typeof(T), 0, true);
                if (window == null || window.Frame == null)
                {
                    throw new NotSupportedException("Can't create LogWindow!?");
                }
                already = window as T;
                IVsWindowFrame frame = window.Frame as IVsWindowFrame;
                if (frame != null)
                {
                    ErrorHandler.ThrowOnFailure(frame.ShowNoActivate());
                }
            }
        }

        void ShowLogWindow()
        {
            CreateToolWindow(ref log_window);
            CreateToolWindow(ref log_settings);
        }

        public static T GetToolWindow<T>() where T : class
        {
            try
            {
                return instance.FindToolWindow(typeof(T), 0, true) as T;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public static Options Options
        {
            get
            {
                return options; 
            }
        }

        public static void LaunchHelpUrl()
        {
            string help_url = "https://bitbucket.org/cskilbeck/log/wiki/Home";
            IVsWebBrowsingService wbSvc = (IVsWebBrowsingService)instance.GetService(typeof(SVsWebBrowsingService));
            Guid guidNull = Guid.Empty;
            uint dwCreateFlags = (uint)__VSCREATEWEBBROWSER.VSCWB_AutoShow | (uint)__VSCREATEWEBBROWSER.VSCWB_StartCustom;
            IVsWebBrowser browser;
            IVsWindowFrame frame;
            wbSvc?.CreateWebBrowser(dwCreateFlags, ref guidNull, "Log Window Help", help_url, null, out browser, out frame);
        }

        void DebuggerStarted(uint process_id)
        {
            FileLog.Write("Debugger started! (PACKAGE)");
            if (Options.ShowOnDebug)
            {
                ShowLogWindow();
            }

            if (LogEntry.LogWindowControl != null)
            {
                LogEntry.TimestampFormatString = Options.TimestampFormat;
                if (Options.ClearOnDebug)
                {
                    LogEntry.LogWindowControl.ClearLog();
                }
                LogEntry.LogWindowControl.auto_scroll = true;
            }
        }

        void DebuggerEnded()
        {
            FileLog.Write("Debugger ended! (PACKAGE)");
            if (Options.ShowOnDebugEnd)
            {
                ShowLogWindow();
            }
        }

        private void Options_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "TimestampFormat":
                    LogEntry.TimestampFormatString = options.TimestampFormat;
                    LogEntry.LogWindowControl?.RefreshLog();
                    break;

                case "Font":
                    LogEntry.LogWindowControl?.SetupFont();
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            IVsSolution solution = GetService(typeof(SVsSolution)) as IVsSolution;
            solution?.UnadviseSolutionEvents(solution_events_cookie);
            base.Dispose(disposing);
            if (disposing)
            {
                mySink.Dispose();
            }
        }

        public void GotoLogEntry(LogEntry e)
        {
            if(e == null)
            {
                return;
            }
            try
            {
                IVsUIHierarchy Hierarchy;
                uint ItemID;
                IVsWindowFrame WindowFrame;
                IVsTextView TextView;
                VsShellUtilities.OpenDocument(ServiceProvider.GlobalProvider,
                                            e.Filename,
                                            Guid.Empty,
                                            out Hierarchy,
                                            out ItemID,
                                            out WindowFrame,
                                            out TextView);
                if (TextView != null)
                {
                    TextSpan span = new TextSpan() { iStartLine = e.LineNumber - 1, iEndLine = e.LineNumber - 1, iStartIndex = 0, iEndIndex = 1 };
                    ErrorHandler.ThrowOnFailure(TextView.EnsureSpanVisible(span));
                    ErrorHandler.ThrowOnFailure(TextView.SetCaretPos(e.LineNumber - 1, 0));
                    ErrorHandler.ThrowOnFailure(TextView.SendExplicitFocus());
                    WindowFrame.Show();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public int SaveUserOptions([In] IVsSolutionPersistence pPersistence)
        {
            pPersistence.SavePackageUserOpts(this, _strSolutionPersistanceKey);
            return VSConstants.S_OK;
        }

        public int WriteUserOptions(IStream pOptionsStream, string pszKey)
        {
            DataStreamFromComStream pStream = new DataStreamFromComStream(pOptionsStream);
            BinaryWriter writer = new BinaryWriter(pStream);
            writer.Write(LogWindowControl.log_channels.Count);
            foreach(Channel c in LogWindowControl.log_channels)
            {
                c.Write(writer);
            }
            return VSConstants.S_OK;
        }

        public int LoadUserOptions(IVsSolutionPersistence pPersistence, uint grfLoadOpts)
        {
            pPersistence.LoadPackageUserOpts(this, _strSolutionPersistanceKey);
            return VSConstants.S_OK;
        }

        public int ReadUserOptions(IStream pOptionsStream, string pszKey)
        {
            DataStreamFromComStream pStream = new DataStreamFromComStream(pOptionsStream);
            BinaryReader reader = new BinaryReader(pStream);
            int channel_count = reader.ReadInt32();
            LogWindowControl.log_channels.Clear();
            for(int i=0; i<channel_count; ++i)
            {
                Channel c = new Channel();
                c.Read(reader);
                LogWindowControl.log_channels.Add(c);
            }
            channels_loaded = true;
            return VSConstants.S_OK;
        }

        public int QuerySaveSolutionProps(IVsHierarchy pHierarchy, VSQUERYSAVESLNPROPS[] pqsspSave)
        {
            return VSConstants.S_OK;
        }

        public int SaveSolutionProps(IVsHierarchy pHierarchy, IVsSolutionPersistence pPersistence)
        {
            return VSConstants.S_OK;
        }

        public int WriteSolutionProps(IVsHierarchy pHierarchy, string pszKey, IPropertyBag pPropBag)
        {
            return VSConstants.S_OK;
        }

        public int ReadSolutionProps(IVsHierarchy pHierarchy, string pszProjectName, string pszProjectMk, string pszKey, int fPreLoad, IPropertyBag pPropBag)
        {
            return VSConstants.S_OK;
        }

        public int OnProjectLoadFailure(IVsHierarchy pStubHierarchy, string pszProjectName, string pszProjectMk, string pszKey)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            FileLog.Write("Solution opened! (PACKAGE)");
            if (!channels_loaded)
            {
                LogWindowControl.log_channels.Clear();
                //LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(0, 200, 0), Enabled = true, Text = "Debug" });
                //LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(0, 200, 200), Enabled = true, Text = "Info" });
                //LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(200, 100, 0), Enabled = true, Text = "Warning" });
                //LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(200, 0, 0), Enabled = true, Text = "Error" });
            }
            return VSConstants.S_OK;
        }

        public int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterCloseSolution(object pUnkReserved)
        {
            channels_loaded = false;
            return VSConstants.S_OK;
        }

        private static string ColorToHex(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private void DumpThemeColors()
        {
#if DEBUG
            Debug.WriteLine("<html><table width=\"100%\">");
            foreach (var prop in typeof(Microsoft.VisualStudio.PlatformUI.EnvironmentColors).GetProperties())
            {
                try
                {
                    ThemeResourceKey r = (ThemeResourceKey)prop.GetValue(null);
                    if (r.KeyType == ThemeResourceKeyType.BackgroundColor)
                    {
                        Debug.Write("<tr>");
                        Debug.Write("<td width=\"20%\">" + prop.Name + "</td>");
                        System.Drawing.Color c = Microsoft.VisualStudio.PlatformUI.VSColorTheme.GetThemedColor(r);
                        Debug.Write("<td width=\"80%\" bgcolor=\"" + ColorToHex(c) + "\">&nbsp;</td>");
                        Debug.WriteLine("</tr>");
                    }
                }
                catch (Exception)
                {
                }
            }
            Debug.WriteLine("</table></html>");
#endif
        }
    }
}

// Yellow: ScreenTipBackgroundColorKey
// Orange: ExtensionManagerStarHighlight1ColorKey
// Greeny yellow: VizSurfaceGoldMediumColorKey
// Pale salmon: VizSurfaceRedLightColorKey
// Pale blue: VizSurfaceSoftBlueLightColorKey
