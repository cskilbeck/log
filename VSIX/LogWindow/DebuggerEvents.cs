﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace LoggingWindow
{
    public class DebuggerEvents : DisposableObject, IVsDebuggerEvents
    {
        bool started;
        private IVsDebugger debugger;
        private uint debugEventsCookie = VSConstants.VSCOOKIE_NIL;

        public delegate void DebuggerStartedDelegate(uint process_id);
        public delegate void DebuggerEndedDelegate();

        public event DebuggerStartedDelegate DebuggerStarted;
        public event DebuggerEndedDelegate DebuggerEnded;

        public virtual void OnDebuggerStarted(uint process_id)
        {
            FileLog.Write("OnDebuggerStarted");
            DebuggerStarted?.Invoke(process_id);
        }

        public virtual void OnDebuggerEnded()
        {
            FileLog.Write("OnDebuggerEnded");
            DebuggerEnded?.Invoke();
        }

        public DebuggerEvents(IVsDebugger debugger_instance)
        {
            FileLog.Write("debugger_instance = {0}", debugger_instance.ToString());
            debugger = debugger_instance;
            debugger.AdviseDebuggerEvents(this, out debugEventsCookie);
            FileLog.Write("DebuggerEvents() - debugger.AdviseDebuggerEvents");
        }

        protected override void DisposeManagedResources()
        {
            ThreadHelper.Generic.Invoke(() =>
            {
                if (debugEventsCookie != VSConstants.VSCOOKIE_NIL && debugger != null)
                {
                    ErrorHandler.CallWithCOMConvention(() => debugger.UnadviseDebuggerEvents(debugEventsCookie));
                    debugEventsCookie = VSConstants.VSCOOKIE_NIL;
                }
            });
            base.DisposeManagedResources();
        }

        public int OnModeChange(DBGMODE dbgmodeNew)
        {
            if (dbgmodeNew == DBGMODE.DBGMODE_Design)
            {
                Stop();
            }
            else
            {
                Start();
            }
            return 0;
        }

        public void Start()
        {
            FileLog.Write("Start()");
            if (!started)
            {
                ExceptionTrigger.Instance.notified = false;
                ExceptionTrigger.Instance.DebuggerStarted += Callback_DebuggerStarted;
                debugger.AdviseDebugEventCallback(ExceptionTrigger.Instance);
                FileLog.Write("debugger.AdviseDebugEventCallback");
                started = true;
            }
        }

        private void Callback_DebuggerStarted(uint process_id)
        {
            OnDebuggerStarted(process_id);
        }

        public void Stop()
        {
            FileLog.Write("Stop()");
            if (started)
            {
                OnDebuggerEnded();
                debugger.UnadviseDebugEventCallback(ExceptionTrigger.Instance);
                FileLog.Write("debugger.UnadviseDebugEventCallback");
                started = false;
            }
        }
    }
}
