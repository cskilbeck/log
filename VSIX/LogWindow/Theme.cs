﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace LoggingWindow
{
    public class Theme : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public delegate void ThemeChangedDelegate();
        public static event ThemeChangedDelegate ThemeChanged;

        public Theme()
        {
            ThemeChanged += LogChannelsControl_ThemeChanged;
        }

        private void LogChannelsControl_ThemeChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VerboseColor"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DebugColor"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("InfoColor"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("WarningColor"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ErrorColor"));
        }

        public Brush VerboseColor { get { return LogEntry.GetSeverityBrush(Severity.Verbose); } }
        public Brush DebugColor { get { return LogEntry.GetSeverityBrush(Severity.Debug); } }
        public Brush InfoColor2 { get { return LogEntry.GetSeverityBrush(Severity.Info); } }
        public Brush WarningColor { get { return LogEntry.GetSeverityBrush(Severity.Warning); } }
        public Brush ErrorColor { get { return LogEntry.GetSeverityBrush(Severity.Error); } }

        public static void DoThemeChanged()
        {
            ThemeChanged?.Invoke();
        }
    }
}
