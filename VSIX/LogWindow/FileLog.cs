﻿using System;
using System.IO;

namespace LoggingWindow
{
    public static class FileLog
    {
#if DEBUG
        private static bool append = false;
        private static object locker = new object();

        public static void Write(string txt, params object[] args)
        {
            lock (locker)
            {
                string s = string.Format(txt, args);
                string app_data = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                using (StreamWriter stream = new StreamWriter(Path.Combine(app_data, "LogWindow.txt"), append))
                {
                    append = true;
                    stream.WriteLine(s);
                    stream.Flush();
                    stream.Close();
                }
            }
        }
#else
        public static void Write(string txt, params object[] args)
        {
        }
#endif
    }
}
