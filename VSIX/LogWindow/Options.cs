﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace LoggingWindow
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [Guid("1D9ECCF3-5D2F-4112-9B25-264596873DC9")]  // Special guid to tell it that this is a custom Options dialog page, not the built-in grid dialog page.
    public class Options : UIElementDialogPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static System.Drawing.Font default_font = new System.Drawing.Font("Courier New", 8);

        private bool clearOnDebug = true;
        private bool showOnDebug = true;
        private bool showOnDebugEnd = true;
        private System.Drawing.Font font = default_font;
        private string timestamp_format = "HH:mm:ss.ffff";

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [Category("LogWindow")]
        [DisplayName("Clear on Debug")]
        [Description("Clear the LogWindow when debugging starts")]
        [DefaultValue(true)]
        public bool ClearOnDebug
        {
            get { return clearOnDebug; }
            set
            {
                clearOnDebug = value;
                NotifyPropertyChanged("ClearOnDebug");
            }
        }

        [Category("LogWindow")]
        [DisplayName("Show on Debug")]
        [Description("Show the LogWindow when debugging starts")]
        [DefaultValue(true)]
        public bool ShowOnDebug
        {
            get { return showOnDebug; }
            set
            {
                showOnDebug = value;
                NotifyPropertyChanged("ShowOnDebug");
            }
        }

        [Category("LogWindow")]
        [DisplayName("Show on Debug End")]
        [Description("Show the LogWindow when debugging ends")]
        [DefaultValue(true)]
        public bool ShowOnDebugEnd
        {
            get { return showOnDebugEnd; }
            set
            {
                showOnDebugEnd = value;
                NotifyPropertyChanged("ShowOnDebugEnd");
            }
        }

        [Category("LogWindow")]
        [DisplayName("Font")]
        [Description("Font family & size for Log Window")]
        [DefaultValue("Courier New, 8")]
        public string Font
        {
            get { return ConvertToString(font); }
            set
            {
                font = ConvertToFont(value);
                NotifyPropertyChanged("Font");
            }
        }

        [Category("LogWindow")]
        [DisplayName("Timestamp format")]
        [Description("Format string for timestamp")]
        [DefaultValue("HH:mm:ss.ffff")]
        public string TimestampFormat
        {
            get { return timestamp_format; }
            set
            {
                timestamp_format = value;
                NotifyPropertyChanged("TimestampFormat");
            }
        }

        protected override UIElement Child
        {
            get { return new OptionsPage(this); }
        }

        public override void ResetSettings()
        {
            ClearOnDebug = true;
            Font = "Lucida Sans Typewriter, 12";
            TimestampFormat = "HH:mm:ss.ffff";
            base.ResetSettings();
        }

        public static string ConvertToString(System.Drawing.Font font)
        {
            try
            {
                System.Drawing.Font f = font ?? default_font;
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                string s = converter.ConvertToString(font);
                System.Diagnostics.Debug.WriteLine("FONT: [" + s + "]");
                return s;
            }
            catch { System.Diagnostics.Debug.WriteLine("Unable to convert"); }
            return null;
        }

        public static System.Drawing.Font ConvertToFont(string fontString)
        {
            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(System.Drawing.Font));
                return (System.Drawing.Font)converter.ConvertFromString(fontString);
            }
            catch { System.Diagnostics.Debug.WriteLine("Unable to convert"); }
            return default_font;
        }

        public System.Drawing.Font CurrentFont
        {
            get
            {
                return font ?? default_font;
            }
            set
            {
                font = value;
                NotifyPropertyChanged("Font");
            }
        }
    }
}
