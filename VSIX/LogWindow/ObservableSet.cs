﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace LoggingWindow
{
    public class ObservableSet<K, V> : INotifyCollectionChanged, IDictionary<K, V>
    {
        private SortedDictionary<K, V> set = new SortedDictionary<K, V>();

        public V this[K key]
        {
            get
            {
                return set[key];
            }

            set
            {
                set[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return set.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public ICollection<K> Keys
        {
            get
            {
                return set.Keys;
            }
        }

        public ICollection<V> Values
        {
            get
            {
                return set.Values;
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public void OnCollectionChanged(NotifyCollectionChangedAction action)
        {
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void Add(KeyValuePair<K, V> item)
        {
            set.Add(item.Key, item.Value);
            OnCollectionChanged(NotifyCollectionChangedAction.Add);
        }

        public void Add(K key, V value)
        {
            set.Add(key, value);
            OnCollectionChanged(NotifyCollectionChangedAction.Add);
        }

        public void Clear()
        {
            set.Clear();
            OnCollectionChanged(NotifyCollectionChangedAction.Reset);
        }

        public bool Contains(KeyValuePair<K, V> item)
        {
            return set.ContainsKey(item.Key);
        }

        public bool ContainsKey(K key)
        {
            return set.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<K, V>[] array, int arrayIndex)
        {
            foreach (KeyValuePair<K, V> kvp in set)
            {
                array[arrayIndex++] = kvp;
            }
        }

        public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
        {
            return set.GetEnumerator();
        }

        public bool Remove(KeyValuePair<K, V> item)
        {
            bool rc = set.Remove(item.Key);
            OnCollectionChanged(NotifyCollectionChangedAction.Remove);
            return rc;
        }

        public bool Remove(K key)
        {
            bool rc = set.Remove(key);
            OnCollectionChanged(NotifyCollectionChangedAction.Remove);
            return rc;
        }

        public bool TryGetValue(K key, out V value)
        {
            return set.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return set.GetEnumerator();
        }
    }
}
