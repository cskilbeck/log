#include <Windows.h>
#include <tchar.h>
#include <string>
#include <vector>
#include "log.h"
#include "str.h"
#include "StopWatch.h"

using namespace chs::Log;
using chs::$;

#define Log_Foo(txt, ...) Log_Write("Foo", txt, __VA_ARGS__)

#define THREADCOUNT 64

double times[THREADCOUNT];
HANDLE aThread[THREADCOUNT];

DWORD WINAPI ThreadFunc(LPVOID time) {
	UINT_PTR id = reinterpret_cast<UINT_PTR>(time);
	auto chan = $("Thread%d", id);
	StopWatch timer;
	Log_Verbose(chan, "Active");
	double time_taken = timer.Elapsed();
	Log_Debug(chan, "%d took %fms", id, time_taken * 1000.0);
	times[id] = time_taken;
	Sleep(1);
	return 0;
}

void test_threading() {
	Log_W("Hello world from the warning channel! But then the message got longer");
	for (int i = 0; i < THREADCOUNT; ++i) {
		Log_Info($("Thread%d", i), "Creating Thread %d", i);
		DWORD ThreadID;
		aThread[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, reinterpret_cast<LPVOID>((UINT_PTR)i), 0, &ThreadID);
	}
	Log_V("Threads are in flight");
	DWORD w = WaitForMultipleObjects(THREADCOUNT, aThread, TRUE, INFINITE);
	if (w == -1) {
		Log_E("Error waiting! %08x", GetLastError());
	}
	Log_V("Threads are complete");
	for (int i = 0; i < THREADCOUNT; i++) {
		CloseHandle(aThread[i]);
	}
	double best_time = times[0];
	for (int i = 1; i < THREADCOUNT; ++i) {
		if (times[i] < best_time) {
			best_time = times[i];
		}
	}
	Log_Write(Severity::Warning, $("ERROR%d", 1), "The error happened in %s", "SOME PLACE");
	Log_V("DONE With threading");
}
