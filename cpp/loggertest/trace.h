#pragma once

// yuck

#if !defined(_APISETDEBUG_)
#if defined(__cplusplus)
extern "C" {
#endif
	__declspec(dllimport) void __stdcall OutputDebugStringA(char const *lpOutputString);
	__declspec(dllimport) void __stdcall OutputDebugStringW(wchar_t const *lpOutputString);
#if defined(__cplusplus)
}
#endif
#endif

// outputdebugstring overloads
namespace {
	template <typename T> void _ODS(T str);
	template <> void _ODS<char const *>(char const *str) { OutputDebugStringA(str); }
	template <> void _ODS<wchar_t const *>(wchar_t const *str) { OutputDebugStringW(str); }
	template <> void _ODS<chs::str>(chs::str str) { OutputDebugStringA(str); }
	template <> void _ODS<chs::wstr>(chs::wstr str) { OutputDebugStringW(str); }
	template <typename T> void _ODS(T str) = delete;
}

#define trace(x) _ODS(x)
