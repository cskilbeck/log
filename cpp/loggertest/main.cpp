﻿#include <Windows.h>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include "StopWatch.h"
#include "log.h"
#include "str.h"
#include "trace.h"

using namespace chs::Log;
using chs::$;

class Tasty {
public:
	Tasty() {
		Log_D("It's tasty as hell");
	}

	void foo() {
		Log_D("FOO!!!OOO!!O!");
	}
};

void test() {

	Tasty().foo();

	Log_Write(Severity::Debug, $("Special%d", rand()), "Text: %d", 55);
	Log_Write(Severity::Verbose, "Special", "Text: %d", 55);

	Log_D("Hello");
}

int main(int, wchar_t **) {

	StopWatch first;
	Log_D("Some debug info");
	double first_time = first.Elapsed() * 1000.0;

	StopWatch ods_first;
	OutputDebugStringA("TEST");
	double ods_first_time = ods_first.Elapsed() * 1000.0;

	Log_Debug("Perf", "1st Log: %f", first_time);
	Log_Debug("Perf", "1st ODS: %f", ods_first_time);

	test();

	Log_W("Hello, it's a warning!");

	for (int i = 0; i < 4; ++i) {
		Log_Verbose($("COM%d", i), $("Some com traffic on port %02x", i));
	}

	Log_I("Finished");

	Log_D("Hello");

	using std::thread;
	thread thr[10];
	for (int i = 0; i < 10; ++i) {
		thr[i] = thread([=] {
			Sleep(rand() % 25);
			Log_Debug($("Thread%d", i), "It's in the thread!");
		});
	}

	for (thread &t : thr) { t.join(); }

	Log_D("Yoink?");

	double best = 100;
	for (int i = 0; i < 1000; ++i) {
		StopWatch s;
		OutputDebugStringA("HELLO");
		double elapsed = s.Elapsed();
		if (elapsed < best) {
			best = elapsed;
		}
	}

	double best2 = 100;
	for (int i = 0; i < 1000; ++i) {
		StopWatch s;
		Log_Write(Severity::Verbose, "CHAN", "TXT");
		double elapsed = s.Elapsed();
		if (elapsed < best2) {
			best2 = elapsed;
		}
	}

	Log_Debug("Perf", "ODS best was %f", best * 1000.0);
	Log_Debug("Perf", "Log best was %f", best2 * 1000.0);

	Log_Write(Severity::Verbose, "Main", "Test");
	Log_Write(Severity::Debug, "Main", "Test");
	Log_Write(Severity::Info, "Main", "Test");
	Log_Write(Severity::Warning, "Main", "Test");
	Log_Write(Severity::Error, "Main", "Test");

	trace("main() exits...\n");

	return 0;
}
