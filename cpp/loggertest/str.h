#pragma once

namespace chs {

	// simple text buffer for the $() function to return
	// it uses malloc(), deal with it (or change it, whatever man)

	template<typename T> class str_base {
	public:
		str_base() {
			buf = nullptr;
		}
		str_base(size_t s) {
			buf = reinterpret_cast<T *>(malloc(s + 1));
		}
		str_base(str_base && other) {
			buf = other.buf;
			other.buf = nullptr;
		}
		str_base &operator=(str_base &&other) {
			if (this != &other) {
				free(buf);
				buf = other.buf;
				other.buf = nullptr;
			}
			return *this;
		}
		T *buffer() const {
			return buf;
		}
		operator T const * () const {
			return buf;		// naughty but nice
		}
		~str_base() {
			if (buf != nullptr) {
				free(buf);
			}
		}
		size_t size() const {
			return buf == nullptr ? 0 : _msize(buf);
		}
		str_base duplicate() const {
			auto my_size = size();
			str_base t(my_size);
			memcpy(t.buffer(), my_size, buf);
			return t;
		}
		// no lvalue copying! use duplicate!
		str_base &operator=(str_base &other) = delete;
	private:
		T *buf;
	};

	using str = str_base<char>;
	using wstr = str_base<wchar_t>;

	// $ is a bit like C# string interpolation

	inline str $(char const *fmt, ...) {
		va_list v;
		va_start(v, fmt);
		size_t l = (size_t)(_vscprintf(fmt, v));
		str n(l + 1);
		if (l > 0) {
			_vsnprintf_s(n.buffer(), l + 1, l, fmt, v);
		}
		return n;
	}

	inline wstr $(wchar_t const *fmt, ...) {
		va_list v;
		va_start(v, fmt);
		size_t l = (size_t)(_vscwprintf(fmt, v));
		wstr n(l + 1);
		if (l > 0) {
			_vsnwprintf_s(n.buffer(), l + 1, l, fmt, v);
		}
		return n;
	}
}
