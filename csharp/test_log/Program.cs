﻿#define DEBUG

using System;
using System.Threading;
using Logging;
using System.Diagnostics;

namespace test_log
{
    class Program
    {
        static void Main(string[] args)
        {
            Debug.WriteLine("Hello");

            const string TAG = "Main";

            Log.WriteToFileOption = Log.WriteToFile.Always;
            Log.Filename = "log.txt";

            Log.Debug("Some debug info", TAG);
            Log.Info("Some more info", TAG);

            string g = "HELLO";
            Log.Error($"It's an error: {g}");
            for (int i = 0; i < 50; ++i)
            {
                new Thread((id) =>
                {
                    Log.Write(Log.Severity.Verbose, $"Thread is running: {id}", $"Thread{id}");
                }).Start(i);
            }

            int n = 0;
            bool quit = true;

            while (!quit)
            {
                Console.Write("x to quit:");
                try
                {
                    string s = Console.ReadLine();
                    if(!String.IsNullOrEmpty(s) && s[0] == 'x')
                    {
                        break;
                    }
                }
                catch(Exception e)
                {
                    Log.Error($"They pressed escape: {e.Message}");
                }

                Log.Write(Log.Severity.Verbose, $"They pressed enter {++n} times", "ENTER");
            }
            Log.Verbose("Testing", "Color");
            Log.Debug("Testing", "Color");
            Log.Info("Testing", "Color");
            Log.Warning("Testing", "Color");
            Log.Error("Goodbye", "Color");
        }
    }
}

