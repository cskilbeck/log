﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;

using CMN = System.Runtime.CompilerServices.CallerMemberNameAttribute;
using CFP = System.Runtime.CompilerServices.CallerFilePathAttribute;
using CLN = System.Runtime.CompilerServices.CallerLineNumberAttribute;

namespace Logging
{
    public class Log
    {
        public enum Severity : int
        {
            Verbose = 0,
            Debug = 1,
            Info = 2,
            Warning = 3,
            Error = 4
        }

        public enum WriteToFile
        {
            Never,
            Always,
            IfNoDebugger
        };

        public static WriteToFile WriteToFileOption
        {
            set
            {
                GlobalLog.file_option = value;
            }
        }

        public static string Filename = "log.csv";

        public static void Write(Severity severity, object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(severity, channel, text, f, l);
        }

        public static void Verbose(object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(Severity.Verbose, channel, text, f, l);
        }

        public static void Debug(object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(Severity.Debug, channel, text, f, l);
        }

        public static void Info(object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(Severity.Info, channel, text, f, l);
        }

        public static void Warning(object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(Severity.Warning, channel, text, f, l);
        }

        public static void Error(object text, [CMN] string channel = "", [CFP] string f = "", [CLN] int l = 0)
        {
            GlobalLog.Output(Severity.Error, channel, text, f, l);
        }

        private const int MAJOR_VERSION = 1;
        private const int MINOR_VERSION = 1;

        // ServerAddress of the debugger port for receiving log messages
        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 20)]
        private struct ServerAddress
        {
            public ServerAddress(byte family, byte version, ushort port, IPAddress address)
            {
                if(family != (byte)AddressFamily.InterNetwork &&
                    family != (byte)AddressFamily.InterNetworkV6)
                {
                    throw new NotSupportedException("Only IPV4 or IPV6 please");
                }
                this.family = family;
                this.version = version;
                this.port = port;
                this.address = new byte[16];
                byte[] addr = address.GetAddressBytes();
                for (int i = 0; i < addr.Length; ++i)
                {
                    this.address[i] = addr[i];
                }
            }

            [MarshalAs(UnmanagedType.U2)]
            public ushort port;
            [MarshalAs(UnmanagedType.U1)]
            public byte family;                 // 0: IPV4, 1: IPV6
            [MarshalAs(UnmanagedType.U1)]       // Major:4 Minor:4
            public byte version;
            [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 16)]
            public byte[] address;
        }

        // log entry packet which precedes the strings in the stream
        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 28)]
        private class LogEntrySender
        {
            [MarshalAs(UnmanagedType.U8)]
            public long timestamp;
            [MarshalAs(UnmanagedType.U4)]
            public int severity;
            [MarshalAs(UnmanagedType.U4)]
            public int line_number;
            [MarshalAs(UnmanagedType.U4)]
            public int channel_length;
            [MarshalAs(UnmanagedType.U4)]
            public int text_length;
            [MarshalAs(UnmanagedType.U4)]
            public int filename_length;
        }

        private WriteToFile file_option = WriteToFile.Never;                    // whether to write to file also
        private bool append_to_log = false;                                     // reset the file each time it gets run
        private LogEntrySender sender = new LogEntrySender();                   // a packet to receive into
        private static int sender_size = Marshal.SizeOf(typeof(LogEntrySender));// which is yay big
        private IntPtr sender_buffer = Marshal.AllocHGlobal(sender_size);       // unmanaged buffer for marshalling
        private byte[] sender_bytes = new byte[65536];                          // main receive buffer
        private object log_window_lock_object = new object();                   // lock to block access from multiple threads
        private object file_lock_object = new object();                         // lock for file writing
        private NetworkStream log_stream = null;                                // stream for sending messages to debugger
        private TcpClient log_client = null;                                    // tcp client for sending messages to debugger
        private static Log GlobalLog = new Log();                               // the one global logger
        private bool no_plugin = false;
        const byte version = (MAJOR_VERSION << 4) | MINOR_VERSION;

        // the debugger triggers on this exception
        // the msg string is the address of a buffer for a ServerAddress struct which it fills in

        private class GetIPException : Exception
        {
            public GetIPException(string msg) : base(msg)
            {
            }
        }

        private Log()
        {
        }

        // notify debugger that we're done

        private void CloseLogStream()
        {
            log_stream?.Close();
            log_stream = null;
            log_client?.Close();
            log_client = null;
        }

        // get the server address of the debugger

        private NetworkStream OpenLogStream()
        {
            // ah yes, but what if they have their own one and want to send log messages from it? eh? eh?
            AppDomain.CurrentDomain.ProcessExit += (s, e) => { CloseLogStream(); };

            // buffer that debugger will write into
            IntPtr server_address_buffer = Marshal.AllocHGlobal(Marshal.SizeOf<ServerAddress>());

            try
            {
                // notify debugger that we want the ServerAddress
                throw new GetIPException(server_address_buffer.ToString());
            }
            catch (GetIPException)
            {
                // server_address_buffer has been filled in now, as if by magic
                ServerAddress server_address = (ServerAddress)Marshal.PtrToStructure(server_address_buffer, typeof(ServerAddress));
                Marshal.FreeHGlobal(server_address_buffer);


                if (server_address.family != (byte)AddressFamily.InterNetwork && server_address.family != (byte)AddressFamily.InterNetworkV6 ||
                    server_address.port < 1000 ||
                    server_address.version != 0x11)
                {
                    no_plugin = true;
                    return null;
                }

                // open up the tcp socket
                AddressFamily family = (AddressFamily)server_address.family;
                int address_size = (family == AddressFamily.InterNetworkV6) ? 16: 4;
                byte[] ip_address_bytes = new byte[address_size];
                Array.Copy(server_address.address, ip_address_bytes, address_size);
                IPAddress address = new IPAddress(ip_address_bytes);
                log_client = new TcpClient(family);
                log_client.Connect(address, server_address.port);
                return log_client.GetStream();
            }
        }

        // CSV admin

        private static char[] characters_that_must_be_quoted = { ',', '"', '\n' };
        private const string quote = "\"";
        private const string double_quote = "\"\"";

        // 1. double any quotes
        // 2. if there's a comma, quote or newline in it, enclose it in single quotes
        // thanks for the Nth time, Stack Overflow

        private static string Escape(string s)
        {
            s = s.Replace(quote, double_quote);
            if (s.IndexOfAny(characters_that_must_be_quoted) > -1)
            {
                s = quote + s + quote;
            }
            return s;
        }

        // Write a log entry
        private void Output(Severity severity, string channel, object text, [CFP] string filePath = "", [CLN] int lineNumber = 0)
        {
            DateTime now = DateTime.Now;
            bool being_debugged = Debugger.IsAttached;

            // write to log file, maybe
            if (file_option == WriteToFile.Always || file_option == WriteToFile.IfNoDebugger && !being_debugged)
            {
                lock (file_lock_object)
                {
                    try
                    {
                        using (StreamWriter stream = new StreamWriter(Filename, append_to_log))
                        {
                            append_to_log = true;
                            const string timestamp_export_format_string = "yyyy-MM-dd,hh:mm:ss.fffff";

                            string line = string.Format("{0},{1},{2},{3},{4},{5}", now.ToString(timestamp_export_format_string),
                                                                                    severity.ToString(),
                                                                                    Escape(channel),
                                                                                    Escape(text.ToString()),
                                                                                    Escape(filePath),
                                                                                    lineNumber);
                            stream.WriteLine(line);
                            stream.Flush();
                            stream.Close();
                        }
                    }
                    catch (IOException ex)
                    {
                        System.Diagnostics.Debug.WriteLine($"Can't write Log Message to {Filename} ({ex.Message}), switching off file logging.");
                        file_option = WriteToFile.Never;
                    }
                }
            }


            // not LogWindow if not running in the debugger
            if (being_debugged && !no_plugin)
            {
                // one at a time, gents
                lock (log_window_lock_object)
                {
                    // fire up the network stream
                    if (log_stream == null)
                    {
                        log_stream = OpenLogStream();
                    }
                    if (log_stream != null)
                    {
                        string text_text = text.ToString();

                        // create a blob of descriptor header
                        sender.timestamp = now.ToFileTime();
                        sender.severity = (int)severity;
                        sender.line_number = lineNumber;
                        sender.channel_length = channel.Length;
                        sender.text_length = text_text.Length;
                        sender.filename_length = filePath.Length;

                        // put the header at the start of the buffer
                        Marshal.StructureToPtr(sender, sender_buffer, true);
                        Marshal.Copy(sender_buffer, sender_bytes, 0, sender_size);

                        // then the text
                        int t1 = Encoding.UTF8.GetBytes(channel, 0, channel.Length, sender_bytes, sender_size);
                        int t2 = Encoding.UTF8.GetBytes(text_text, 0, text_text.Length, sender_bytes, sender_size + t1);
                        int t3 = Encoding.UTF8.GetBytes(filePath, 0, filePath.Length, sender_bytes, sender_size + t1 + t2);

                        // send it
                        log_stream.Write(sender_bytes, 0, sender_size + t1 + t2 + t3);
                    }
                }
            }
            else 
            {
                CloseLogStream();
            }
        }
    }
}
